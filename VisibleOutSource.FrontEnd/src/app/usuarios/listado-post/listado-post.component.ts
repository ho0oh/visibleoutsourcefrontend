import { Component, OnInit } from '@angular/core';
import { PostService } from '../post.service';
import { postDTO } from '../post';

@Component({
  selector: 'app-listado-post',
  templateUrl: './listado-post.component.html',
  styleUrls: ['./listado-post.component.css']
})
export class ListadoPostComponent implements OnInit {

  constructor(private postsService: PostService) { }

  posts: postDTO[];
  columnasAMostrar = ['userId', 'id', 'title', 'body']

  ngOnInit(): void {
    this.postsService.obtenerTodos()
    .subscribe(posts => {
      this.posts = posts;
    }, error => console.error(error));
  }

}
