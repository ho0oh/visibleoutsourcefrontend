export interface postDTO{
    userId: number;
    id: number;
    title: string;
    body: string;
}