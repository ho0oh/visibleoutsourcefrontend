import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { postDTO } from './post';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  private apiURL = environment.apiURL + 'Post';

  public obtenerTodos() : Observable<postDTO[]>{
    return this.http.get<postDTO[]>(this.apiURL);
  }
}
