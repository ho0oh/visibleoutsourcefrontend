import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { ListadoLogsComponent } from './logs/listado-logs/listado-logs.component';
import { ListadoPostComponent } from './usuarios/listado-post/listado-post.component';

const routes: Routes = [
  {path: '', component: LandingPageComponent},
  {path: 'logs', component: ListadoLogsComponent},
  {path: 'posts', component: ListadoPostComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
