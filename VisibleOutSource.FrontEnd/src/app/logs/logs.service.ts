import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { logsDTO } from './log';

@Injectable({
  providedIn: 'root'
})
export class LogsService {

  constructor(private http: HttpClient) { }

  private apiURL = environment.apiURL + 'Log/DataLog';

  public obtenerTodos() : Observable<logsDTO[]>{
    //return [{id: 1, errorMessage: 'error1', errorStackTrace: 'error2', date: Date.now.toString()}]
    return this.http.get<logsDTO[]>(this.apiURL);
  }
}
