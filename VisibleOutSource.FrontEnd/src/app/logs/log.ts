export interface logsDTO{
    id: number;
    errorMessage: string;
    errorStackTrace: string;
    date: string;
}