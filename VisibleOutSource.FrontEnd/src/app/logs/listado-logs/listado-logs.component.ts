import { Component, OnInit } from '@angular/core';
import { Subscriber } from 'rxjs';
import { logsDTO } from '../log';
import { LogsService } from '../logs.service';

@Component({
  selector: 'app-listado-logs',
  templateUrl: './listado-logs.component.html',
  styleUrls: ['./listado-logs.component.css']
})
export class ListadoLogsComponent implements OnInit {

  constructor(private logsService: LogsService) { }

  logs: logsDTO[];
  columnasAMostrar = ['id', 'errorMessage', 'errorStackTrace', 'date']

  ngOnInit(): void {
    this.logsService.obtenerTodos()
    .subscribe(logs => {
      this.logs = logs;
    }, error => console.error(error));
    
  }

}
